package pl.sda.cookBook.Cookbook.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pl.sda.cookBook.Cookbook.entity.Recipe;
import pl.sda.cookBook.Cookbook.entity.Ingredient;
import pl.sda.cookBook.Cookbook.model.RecipeModel;
import pl.sda.cookBook.Cookbook.repositories.RecipeRepository;

import java.util.List;


@RestController
@RequestMapping("/recipe")

public class RecipeController {
    @Autowired
    private RecipeRepository recipeRepository;

    @GetMapping("/list")
    public String getList(Model model) {
        model.addAttribute("recipelist", recipeRepository.findAll());
        return "recipelist";
    }

    @GetMapping("/add")
    public String addRecipe(Model model) {
        RecipeModel recipeModel = new RecipeModel();
        model.addAttribute("newRecipe", recipeModel);
        return "newRecipe";
    }

    @GetMapping("/remove/{recipeId}")
    public String removeRecipe(@PathVariable("recipeId") long recipeId) {
        if (recipeRepository.findOne(recipeId) == null) {
            return "Przepis nie istnieje";
        }
        recipeRepository.delete(recipeId);
        return "redirect:/recipe";
    }

    @PostMapping(path = "/createNew")
    public String createNewRecipe(@Validated @ModelAttribute("newRecipe")
                                          RecipeModel newRecipe, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "newRecipe";
        }

        Recipe entity = Recipe.builder()
                .name(newRecipe.getName())
                .ingredients(newRecipe.getIngredients())
                .recipe(newRecipe.getRecipe())
                .build();

        recipeRepository.save(entity);
        return "redirect:/recipe";
    }

    @GetMapping(path = "/searchbyingredient")
    public String searchByIngredient(Model model, String ingr) {
        Ingredient ingredient = new Ingredient();
        model.addAttribute("addIngredient", ingredient.addIngredientToList(ingr));
        if (ingredient.getIngredientList().size() == 5) {
        ingredient.setIngredient(ingredient.getIngredientList().get(1));
        String ingr1 = ingredient.getIngredient();
        ingredient.setIngredient(ingredient.getIngredientList().get(2));
        String ingr2 = ingredient.getIngredient();
        ingredient.setIngredient(ingredient.getIngredientList().get(3));
        String ingr3 = ingredient.getIngredient();
        ingredient.setIngredient(ingredient.getIngredientList().get(4));
        String ingr4 = ingredient.getIngredient();
        ingredient.setIngredient(ingredient.getIngredientList().get(5));
        String ingr5 = ingredient.getIngredient();
        model.addAttribute("by5ingr",recipeRepository.selectRecipeBy5Ingredient
                (ingr1,ingr2,ingr3,ingr4,ingr5));
        return "/searchbyingredient/by5ingr";
        }else         if (ingredient.getIngredientList().size() == 4) {
            ingredient.setIngredient(ingredient.getIngredientList().get(1));
            String ingr1 = ingredient.getIngredient();
            ingredient.setIngredient(ingredient.getIngredientList().get(2));
            String ingr2 = ingredient.getIngredient();
            ingredient.setIngredient(ingredient.getIngredientList().get(3));
            String ingr3 = ingredient.getIngredient();
            ingredient.setIngredient(ingredient.getIngredientList().get(4));
            String ingr4 = ingredient.getIngredient();
        model.addAttribute("by4ingr",recipeRepository.selectRecipeBy4Ingredient(ingr1,ingr2,ingr3,ingr4));
        return "/searchbyingredient/by4ingr";
        }else         if (ingredient.getIngredientList().size() == 3) {
            ingredient.setIngredient(ingredient.getIngredientList().get(1));
            String ingr1 = ingredient.getIngredient();
            ingredient.setIngredient(ingredient.getIngredientList().get(2));
            String ingr2 = ingredient.getIngredient();
            ingredient.setIngredient(ingredient.getIngredientList().get(3));
            String ingr3 = ingredient.getIngredient();
        model.addAttribute("by3ingr",recipeRepository.selectRecipeBy3Ingredient(ingr1,ingr2,ingr3));
        return "/searchbyingredient/by3ingr";
        }
        else if (ingredient.getIngredientList().size() == 2) {
            ingredient.setIngredient(ingredient.getIngredientList().get(1));
            String ingr1 = ingredient.getIngredient();
            ingredient.setIngredient(ingredient.getIngredientList().get(2));
            String ingr2 = ingredient.getIngredient();
            model.addAttribute("by2ingr",recipeRepository.selectRecipeBy2Ingredient(ingr1,ingr2));
            return "/searchbyingredient/by2ingr";
        }
        else if (ingredient.getIngredientList().size() == 1) {
            ingredient.setIngredient(ingredient.getIngredientList().get(1));
            String ingr1 = ingredient.getIngredient();
            model.addAttribute("by1ingr",recipeRepository.selectRecipeBy1Ingredient(ingr1));
            return "/searchbyingredient/by1ingr";
        }
        return "Brak składników w filtrze";
    }

    //////// Rest api for testing


    @RequestMapping(value = "/createNewRecipe", method = RequestMethod.POST)
    public ResponseEntity<String> CreateNewRecipe(@RequestBody RecipeModel newRecipe) {
        Recipe entity = Recipe.builder()
                .name(newRecipe.getName())
                .ingredients(newRecipe.getIngredients())
                .recipe(newRecipe.getRecipe())
                .build();
        recipeRepository.save(entity);
        return ResponseEntity.ok("recipe added");
    }

    @RequestMapping(value = "/searchbyingredients", method = RequestMethod.POST)
    public ResponseEntity<List<Recipe>> searchByIngredients(@RequestBody String ingredient) {
        Ingredient ingredientQuery = new Ingredient();
        ingredientQuery.setIngredient(ingredient);
        List<Recipe> result = recipeRepository.selectRecipeBy1Ingredient(ingredientQuery.getIngredient());
        return ResponseEntity.ok(result);
    }


    @RequestMapping(value = "/recipelist", method = RequestMethod.GET)
    public ResponseEntity<List<Recipe>> recipeList() {
        List<Recipe> result = (List<Recipe>) recipeRepository.findAll();
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public ResponseEntity<String> deleteRecipe(@PathVariable("id") Long recipeId) {
        if (recipeRepository.findOne(recipeId) == null) {
            return ResponseEntity.ok("Przepis nie istnieje");
        }
        recipeRepository.delete(recipeId);
        return ResponseEntity.ok("przepis usunieto");
    }

    @RequestMapping(value = "/recipebyid/{id}", method = RequestMethod.GET)
    public ResponseEntity<Recipe> recipe(@PathVariable("id") Long id) {
        if (recipeRepository.findOne(id) == null) {
            return ResponseEntity.ok(null);
        }
        return ResponseEntity.ok(recipeRepository.findOne(id));
    }
}



