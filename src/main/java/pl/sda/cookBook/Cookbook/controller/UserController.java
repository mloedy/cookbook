package pl.sda.cookBook.Cookbook.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pl.sda.cookBook.Cookbook.entity.MyUser;
import pl.sda.cookBook.Cookbook.entity.Recipe;
import pl.sda.cookBook.Cookbook.model.RecipeModel;
import pl.sda.cookBook.Cookbook.model.UserModel;
import pl.sda.cookBook.Cookbook.repositories.UserRepository;

import javax.jws.soap.SOAPBinding;

@RestController
@RequestMapping("/myUser")
public class UserController {
    @Autowired
    private UserRepository userRepository;
    @GetMapping("/list")
    public String getList(Model model) {
        model.addAttribute("userlist", userRepository.findAll());
        return "userlist";
    }
    @GetMapping("/remove/{recipeId}")
    public String removeUser(@PathVariable("userId") long userId) {
        if (userRepository.findOne(userId) == null) {
            return "Przepis nie istnieje";
        }
        userRepository.delete(userId);
        return "redirect:/myUser";
    }
    @PostMapping(path = "/createNew")
    public String createNewUser(@Validated @ModelAttribute("newUser")
                                        UserModel newUser, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "newRecipe";
        }

        MyUser entity = MyUser.builder().username(newUser.getUsername())
                .password(newUser.getPassword())
                .build();
        userRepository.save(entity);
        return "redirect:/recipes";
    }
    @RequestMapping(value = "/createNew2", method = RequestMethod.POST)
    public ResponseEntity<String> addUser(@RequestBody UserModel newUser){
        MyUser entity = MyUser.builder().username(newUser.getUsername())
                .password(newUser.getPassword())
                .build();
        userRepository.save(entity);
        return ResponseEntity.ok("user added");
    }
}
