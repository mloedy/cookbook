package pl.sda.cookBook.Cookbook.entity;

import java.util.List;

public class Ingredient {
    private String ingredient;

    public String getIngredient() {
        return "%"+ingredient+"%";
    }

    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }

    private List<String> ingredientList;

    public List<String> getIngredientList() {
        return ingredientList;
    }

    public void setIngredientList(List<String> ingredientList) {
        this.ingredientList = ingredientList;
    }

    public String addIngredientToList(String ingredient){
        ingredientList.add(ingredient);
        return "added";
    }
}
