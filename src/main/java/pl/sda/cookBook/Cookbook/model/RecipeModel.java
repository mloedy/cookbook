package pl.sda.cookBook.Cookbook.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.stereotype.Component;
import pl.sda.cookBook.Cookbook.entity.Recipe;

import javax.validation.constraints.Size;
import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class RecipeModel {


    private Long id;
    @NotEmpty
    @Size(max=40)
    private String name;
    @NotEmpty
    @Size(max=999)
    private String recipe;
    @NotEmpty
    private String ingredients;
    public RecipeModel(Recipe entity){
        this.id = entity.getId();
        this.name = entity.getName();
        this.recipe = entity.getRecipe();
        this.ingredients = entity.getIngredients();
    }
}
