package pl.sda.cookBook.Cookbook.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserModel {
    private Long id;
    @NotEmpty
    @Size(max = 20)
    private String username;
    @NotEmpty
    @Size(max = 20)
    private String password;

}
