package pl.sda.cookBook.Cookbook.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.sda.cookBook.Cookbook.entity.MyUser;
@Repository
public interface UserRepository extends CrudRepository<MyUser, Long>{
}
