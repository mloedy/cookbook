package pl.sda.cookBook.Cookbook.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.sda.cookBook.Cookbook.entity.Recipe;

import java.util.List;


@Repository
public interface RecipeRepository extends CrudRepository<Recipe, Long>{
    @Query("SELECT r FROM Recipe r where ingredients like :ingredient")
    List<Recipe> selectRecipeBy1Ingredient(@Param("ingredient") String ingredient);
    @Query("SELECT r FROM Recipe r where ingredients like :ingredient and :ingredient2")
    List<Recipe> selectRecipeBy2Ingredient(@Param("ingredient") String ingredient
            ,@Param("ingredient2") String ingredient2);
    @Query("SELECT r FROM Recipe r where ingredients like :ingredient and :ingredient2 and :ingredient3")
    List<Recipe> selectRecipeBy3Ingredient(@Param("ingredient") String ingredient
            ,@Param("ingredient2") String ingredient2
            ,@Param("ingredient3") String ingredient3);
    @Query("SELECT r FROM Recipe r where ingredients like :ingredient and :ingredient2 and :ingredient3 and :ingredient4")
    List<Recipe> selectRecipeBy4Ingredient(@Param("ingredient") String ingredient
            ,@Param("ingredient2") String ingredient2
            ,@Param("ingredient3") String ingredient3
            ,@Param("ingredient4") String ingredient4);
    @Query("SELECT r FROM Recipe r where ingredients like :ingredient and :ingredient2 and :ingredient3" +
            " and :ingredient4 and :ingredient5")
    List<Recipe> selectRecipeBy5Ingredient(@Param("ingredient") String ingredient
            ,@Param("ingredient2") String ingredient2
            ,@Param("ingredient3") String ingredient3
            ,@Param("ingredient4") String ingredient4
            ,@Param("ingredient5") String ingredient5);

}

